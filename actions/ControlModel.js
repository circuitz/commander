'use strict';
const { Action, api } = require('actionhero');
const request = require('request');
const rp = require('request-promise-native');

module.exports = class MyAction extends Action {
  constructor() {
    super();
    this.name = 'ControlModel';
    this.description = 'an actionhero action';
    this.outputExample = {};
    this.inputs = {
      serial: {
        required: true
      },
      command: {
        required: true
      }
    };
  }

  async run(data) {
    const { req, res, parsedURL } = data.connection.rawConnection;
    const { kitnode } = api.database.models;
    const record = await kitnode.findOne({
      where: {
        serial: data.params.serial
      }
    });

    if (!record) {
      data.connection.setStatusCode(404);
      throw new Error('model not found');
    }

    const route = '/status';

    const uri = `http://${record.ip}:8777${route}`;

    data.response = await rp({
      method: 'get',
      uri,
      json: true,
      timeout: 1000
    });
  }
};
