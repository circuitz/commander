'use strict';

const CIDR = require('cidr-js');
const cidr = new CIDR();
const rp = require('request-promise-native');


class ModelScanner {
  constructor(options = { timeout: 1000 }) {
    this.options = options;
  }

  async scan(port, range, route) {
    const ips = cidr.list(range);

    const options = {
      json: true,
      timeout: this.options.timeout,
      method: 'get'
    };
    const promises = [];

    for (const ip of ips) {
      const o = Object.assign({
        uri: `http://${ip}:${port}${route}`
      }, options);
      const p = rp(o).then(r => {
        return { [ip]: r };
      }).catch(e => {});
      promises.push(p);
    }
    const results = await Promise.all(promises);
    let result = {};
    for (const r of results) {
      if (!r) continue;
      result = { ...result, ...r };
    }

    return result;
  }
}

module.exports = ModelScanner;
