'use strict';

const rp = require('request-promise-native');

class KitNodeClient {
  constructor(ip, port) {
    this._ip = ip;
    this._port = port;
  }

  _http(method, route, query = {}, body = {}) {
    return rp({
      method,
      qs: query,
      body,
      uri: `http://${this._ip}:${this._port}${route}`,
      json: true
    });
  }

  getStatus() {
    return this._http('get', '/status');
  }
}

module.exports = KitNodeClient;
