'use strict';

const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const path = require('path');

const filename = 'circuitz.sqlite';

exports['default'] = {
  database: (api) => {
    return {
      db:      'circuitz',
      user:    'circuitz',
      pass:    '!circuitz123',
      options: {
        host:        'localhost',
        dialect:     'sqlite',
        operatorsAliases: Op,
        pool:        {
          max:  5,
          min:  0,
          idle: 10000
        },
        storage:     path.join(__dirname, '../data/', filename), // ':memory',
        transaction: Sequelize.Transaction.DEFERRED, // 'DEFERRED', 'IMMEDIATE', 'EXCLUSIVE'
        benchmark:   false,
        logging:     api.log,
        retry: {
          match: [
            'SQLITE_BUSY',
            'SQLITE_BUSY: database is locked'
          ],
          max:   15
        }
      }
    };
  }
};

exports['test'] = {
  database: (api) => {
    return {

    };
  }
};
