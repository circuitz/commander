'use strict';

const MODE = process.env.MODE === 'HUB' ? 'HUB' : 'MODEL';

exports['default'] = {
  modelscanner: (api) => {
    return {
      mode: MODE
    }
  }
};
