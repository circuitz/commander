'use strict';

const SECONDS_IN_A_DAY = 60 * 60 * 24;

exports['default'] = {
  kitnodes: api => {
    return {
      expireAfterSeconds: SECONDS_IN_A_DAY,
      network: '10.0.1.0/24',
      port: 8777
    };
  }
};

exports['test'] = {
  kitnodes: api => {
    return {};
  }
};
