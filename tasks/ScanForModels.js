'use strict';
const ActionHero = require('actionhero');
const {api} = require('actionhero');
const ModelScanner = require('../lib/ModelScanner');
const uuid = require('uuid');

const PORT = 8777;

module.exports = class ScanForModels extends ActionHero.Task {
  constructor() {
    super();
    this.name = 'ScanForModels';
    this.description = 'Scan for models on the network';
    this.frequency = 5000;
    this.queue = 'default';
    this.middleware = []
  }

  async run(data) {
    // your logic here
    try {
      const start = Date.now();

      const scanner = new ModelScanner({
        timeout: 250
      });

      const { port, network } = api.config.kitnodes;

      api.log('Scan started', 'info', null);
      let result = await scanner.scan(port, network, '/status');

      const end = Date.now();

      api.log(`Scan completed in ${end - start} ms`, 'info', null);

      const { kitnode } = api.database.models;

      const kitnodes = [];

      for (const ip of Object.keys(result)) {
        const entry = result[ip];
        kitnodes.push({
          serial: entry.serial,
          uptime: entry.time,
          ip: ip,
          port: PORT
        });
      }

      for (const entry of kitnodes) {
        const record = await kitnode.findOne({
          where: {
            serial: entry.serial
          }
        });

        if (record) {
          // record found; update it
          await kitnode.update(entry, {
            where: {
              serial: entry.serial
            }
          });
        } else {
          // no record found; create it
          await kitnode.create(Object.assign({
            id: uuid.v4()
          }, entry));
        }
      }
    } catch (e) {
      api.log('Scan failed', 'error', e);
    }
  }
};
