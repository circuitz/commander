'use strict';

const CIDR = require('cidr-js');
const cidr = new CIDR();
const rp = require('request-promise-native');

(async () => {
  const block = '10.0.1.0/24';
  const ips = cidr.list(block);

  const options = {
    json: true,
    timeout: 1000,
    method: 'get'
  };
  const promises = [];

  for (const ip of ips) {

    const o = Object.assign({
      uri: `http://${ip}:8777/status`
    }, options);
    const p = rp(o).then(r => {
      return { [ip]: r };
    }).catch(e => {});
    promises.push(p);
  }
  const results = await Promise.all(promises);
  let result = {};
  for (const r of results) {
    if (!r) continue;
    result = { ...result, ...r };
  }

  console.log(result);
})();
