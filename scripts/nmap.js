'use strict';

const nmap = require('libnmap');
const scanAsync = require('util').promisify(nmap.scan);

const opts = {
  range: [
    '10.0.1.0/24'
  ],
  ports: '8777',
  json: true
};

const processReport = function (item) {
  if (!item.host) return;
  for (const host of item.host) {
    const { addr } = host.address[0].item;
    const { reason, state } = host.status[0].item;
    console.log(`${addr}: ${reason} (${state})`);
  }
};

const run = async function() {
  try {
    const report = await scanAsync(opts);
    const keys = Object.keys(report);
    for (const key of keys) {
      const entry = report[key];
      processReport(entry);
    }
  } catch (e) {
    console.log(e);
  }
};

(async () => {
  await run();
})();
