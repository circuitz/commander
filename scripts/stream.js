'use strict';

const fs = require('fs');
const url = require('url');

async function read(readable) {
  readable.setEncoding('utf8');
  let data = '';
  for await (const k of readable) {
    data += k;
  }
  console.log(data);
}

(async () => {
  read(fs.createReadStream(new URL('/api/status', 'http://localhost:41153'))).catch(console.log);
})();
