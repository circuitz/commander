'use strict';

const { Socket } = require('net');

const checkPortAsync = (port, host) => {
  let socket = new Socket();
  let status = null;

  return new Promise(resolve => {
    socket.on('connect', () => {
      status = 'open';
      socket.end();
    });

    socket.on('timeout', () => {
      status = 'closed';
      socket.destroy();
    });

    socket.on('error', e => {
      status = 'closed';
    });

    socket.on('close', () => {
      resolve({
        status,
        host,
        port
      });
    });

    socket.setTimeout(300);
    socket.connect(port, host);
  });
};

(async () => {
  const LAN = '10.0.1';
  const promises = [];

  for (let i = 2; i < 255; i++) {
    const host = `${LAN}.${i}`;
    promises.push(checkPortAsync(8777, host));
  }

  const result = (await Promise.all(promises)).filter(host => host.status === 'open');
  console.log(result);
})();
