'use strict';

var client;

var boot = function () {
  client = new ActionheroWebsocketClient();

  client.on('connected', function () {
    console.log('connected!')
  });
  client.on('disconnected', function () {
    console.log('disconnected :(')
  });

  client.on('error', function (error) {
    console.log('error', error.stack)
  });
  client.on('reconnect', function () {
    console.log('reconnect')
  });
  client.on('reconnecting', function () {
    console.log('reconnecting')
  });

  // client.on('message',      function(message){ console.log(message) })

  client.on('alert', function (message) {
    alert(JSON.stringify(message))
  });
  client.on('api', function (message) {
    alert(JSON.stringify(message))
  });

  client.on('welcome', function (message) {
    console.log(message);
  });
  client.on('say', function (message) {
    console.log(message);
  });

  client.connect(function (error, details) {
    if (error) {
      console.error(error);
    } else {
      client.action('createChatRoom', {name: 'defaultRoom'}, function (data) {
        client.roomAdd("defaultRoom");
        // document.getElementById("name").innerHTML = "<span style=\"color:#" + intToARGB(hashCode(client.id)) + "\">" + client.id + "</span>";
        // document.getElementById("fingerprint").innerHTML = client.fingerprint;
      });
    }
  });

};

var sendMessage = function () {
  var div = document.getElementById("message");
  var message = div.value;
  div.value = "";
  client.say(client.rooms[0], message);
};
