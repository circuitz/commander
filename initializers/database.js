'use strict';

const {api, Initializer} = require('actionhero');
const Sequelize = require('sequelize');
const path = require('path');
const fs = require('fs');

module.exports = class DatabaseInitializer extends Initializer {
  constructor() {
    super();
    this.name = 'database';
    this.loadPriority = 1000;
    this.startPriority = 1000;
    this.stopPriority = 1000
  }

  async loadModels(db) {
    let baseDir = path.join(__dirname, '..', 'db', 'models');
    api.log(`Loading models in ${baseDir}`);
    let files = fs.readdirSync(baseDir);

    for (const file of files) {
      let filepath = path.join(baseDir, file);
      if (fs.lstatSync(filepath).isFile()) {
        api.log(`...loading ${file}`);
        db.import(filepath);
      }
    }

    let models = Object.keys(db.models);

    const promises = [];

    for (const model of models) {
      promises.push(db.models[model].sync());
    }

    return Promise.all(promises);
  }

  async initialize() {
    let cfg = api.config.database;
    let database = new Sequelize(cfg.db, cfg.user, cfg.pass, cfg.options);
    await database.authenticate();
    await this.loadModels(database);
    api.database = database;
  }

  async start() {
  }

  async stop() {
  }
};
