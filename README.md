# model-manager

### Task "ScanForModels"

1. Scan for models in the network
2. Lock cache
3. For each result, store in cache with TTL same as task frequency
4. Unlock cache
4. For each model, call 

### Action "GetModels"

GET /models

1. Lock cache
2. Load models from cache
3. Unlock cache
4. Return array of models

### Action "ControlModel"

POST /models/{ip}/{command}?args...

### Action "GetModelInfo"

GET /models/{ip}/info

# Flow

## Scan for new kitnodes

1. Commander scans the network
2. On new data
   1. if the kitnode doesn't exist in the database
      1. add it to the database
      2. push a change to all websocket clients of the addition
   2. otherwise
      1. update the "lastUpdated" timestamp to now
      2. push a change to all websocket clients of the heartbeat
   
## Cleanup old kitnodes

1. For each existing kitnode in the database
   1. if kitnode lastUpdated > 1 day
      1. delete it from the database
      2. push a change to all websocket clients of the deletion   
   
