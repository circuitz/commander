'use strict';

module.exports = function(sequelize, DataTypes) {
  // api.log(`Indexes turned off for reading_property`, 'notice');
  return sequelize.define('settings', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    key: {
      type: DataTypes.STRING,
      field: 'key'
    },
    value: {
      type: DataTypes.STRING,
      field: 'value'
    }
  }, {
    freezeTableName: true,
    timestamps: true,
    indexes: [
      {
        unique: true,
        method: 'BTREE',
        fields: ['id', 'key']
      }
    ]
  });
};
