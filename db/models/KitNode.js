'use strict';

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('kitnode', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      field: 'name'
    },
    serial: {
      type: DataTypes.STRING,
      field: 'serial'
    },
    ip: {
      type: DataTypes.STRING,
      field: 'ip'
    },
    uptime: {
      type: DataTypes.FLOAT,
      field: 'uptime'
    }
  }, {
    freezeTableName: true,
    timestamps: true,
    indexes: [
      {
        unique: true,
        method: 'BTREE',
        fields: ['id', 'name', 'serial']
      }
    ]
  });
};
